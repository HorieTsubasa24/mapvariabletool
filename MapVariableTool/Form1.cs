﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace MapVariableTool
{
    public partial class MainForm : Form
    {
        private const string MASS_PATH = "/Picture/GrayMass.png";

        private readonly Size FORM_DEFAULT_SIZE = new Size(690, 563);
        private readonly Size MAPBOX_DEFAULT_SIZE = new Size(500, 500);
        private readonly Point PROPARTY_DEFAULT_POS = new Point(518, 12);
        private readonly Point BASE_SPACE = new Point(25, 25);

        private const int DEFAULT_MAP_MAX = 11;

        private const string MAPFLOOR_LABEL = "Floor = ";
        private const string MAP_NUMFLOOR = "FloorNum = ";
        private const string MAPSIZE_X_LABEL = "MapSizeX = ";
        private const string MAPSIZE_Y_LABEL = "MapSizeY = ";
        private const string MAPDATA_LABEL = "[MapData]";

        private const int MODE_NUM = 5;
        private const string MAPICONID_TAGNAME = "[MapIconID]";
        private const string ENEMY_PERCENT_TAGNAME = "[ENEMY_PERCENT]";
        private const string ENEMY_LAYER_TAGNAME = "[ENEMY_LAYER]";
        private const string TALK_TAGNAME = "[TALK]";
        private const string DOOR_LOCK_MAP_TAGNAME = "[DOOR_LOCK_MAP]";
        private readonly string[] TAGNAMES = new string[]{ MAPICONID_TAGNAME, ENEMY_PERCENT_TAGNAME, ENEMY_LAYER_TAGNAME, TALK_TAGNAME, DOOR_LOCK_MAP_TAGNAME };

        private const int UP = 0;
        private const int RIGHT = 1;
        private const int DOWN = 2;
        private const int LEFT = 3;

        public static string CurrentPath;
        
        private Image massImage;
        private MassBox[] massBoxes;
        private int MapXStartIndex;
        private int MapYStartIndex;

        private string[] fileContents;
        private byte[] mapDatas;
        private int XStartIndex;
        private int Floor;
        private int X = -1;
        private int Y = -1;
        private int NumFloor;

        private int mode;
        private byte[,,] modeData;

        /// <summary>
        /// フォーム初期化
        /// </summary>
        public MainForm()
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            CurrentPath = assembly.Location;
            string[] words = CurrentPath.Split('\\');
            string removeWord = words[words.Length - 1];
            CurrentPath = CurrentPath.Replace(removeWord, string.Empty);
            InitializeComponent();
            Floor = (int)floorSelecter.Value;
        }

        /// <summary>
        /// ファイルロードダイアログを出す
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileLoadButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                fileContents = File.ReadAllLines(openFileDialog1.FileName);
                InitMapView();
            }
        }

        /// <summary>
        /// マップ表示を初期化する
        /// </summary>
        private void InitMapView()
        {
            bool fileReaded = ReadFile();
            if (!fileReaded)
                return;

            CreateMap();
        }

        /// <summary>
        /// ファイルをロード
        /// </summary>
        private bool ReadFile()
        {
            int index = 0; X = -1; Y = -1; NumFloor = -1; MapXStartIndex = -1; MapYStartIndex = -1;
            while (index < fileContents.Length && (X == -1 || Y == -1 || NumFloor == -1 || MapXStartIndex == -1 || MapYStartIndex == -1))
            {
                if (fileContents[index].Contains(MAPFLOOR_LABEL))
                {
                    string floor = fileContents[index].Replace(MAPFLOOR_LABEL, string.Empty);
                    NumFloor = Convert.ToInt32(floor) + 1;
                    floorSelecter.Maximum = NumFloor;
                }
                else if (fileContents[index].Contains(MAPSIZE_X_LABEL))
                {
                    string x = fileContents[index].Replace(MAPSIZE_X_LABEL, string.Empty);
                    X = Convert.ToInt32(x) + 1;
                }
                else if (fileContents[index].Contains(MAPSIZE_Y_LABEL))
                {
                    string y = fileContents[index].Replace(MAPSIZE_Y_LABEL, string.Empty);
                    Y = Convert.ToInt32(y) + 1;
                }
                else if (fileContents[index].Contains(MAPDATA_LABEL))
                {
                    MapYStartIndex = index + 1;
                    MapXStartIndex = MapYStartIndex + NumFloor * Y;
                    Floor = (int)floorSelecter.Value;
                    LoadMapData(Floor);
                }
                ++index;
            }
            return (X >= 0 && Y >= 0 && NumFloor > 0 && MapXStartIndex > 0 && MapYStartIndex > 0);
        }

        /// <summary>
        /// フロアからマップデータを読み込む
        /// </summary>
        /// <param name="floor"></param>
        private void LoadMapData(int floor)
        {
            floor--;
            mapDatas = new byte[(X + 1) * Y + X * (Y + 1)];

            int index = 0;
            int lineFirst = MapYStartIndex + floor * Y;
            int lineMax = MapYStartIndex + Y + floor * Y - 1;

            int yNum = X + 1;
            for (int i = lineFirst; i <= lineMax; i++)
            {
                string line = fileContents[i];
                for (int j = 0; j < yNum; j++)
                {
                    byte dat = Convert.ToByte(line.Substring(j * 2, 2), 16);
                    mapDatas[index * yNum + j] = dat;
                }
                index++;
            }

            XStartIndex = index * yNum;
            index = 0;
            lineFirst = MapXStartIndex + floor * (Y + 1);
            lineMax = MapXStartIndex + Y + 1 + floor * (Y + 1) - 1;
            int xNum = X;

            for (int i = lineFirst; i <= lineMax; i++)
            {
                string line = fileContents[i];
                for (int j = 0; j < xNum; j++)
                {
                    byte dat = Convert.ToByte(line.Substring(j * 2, 2), 16);
                    mapDatas[XStartIndex + index * xNum + j] = dat;
                }
                index++;
            }
        }

        /// <summary>
        /// マップ表示作成
        /// </summary>
        private void CreateMap()
        {
            modeData = new byte[NumFloor, MODE_NUM, X * Y];

            this.Size = new Size(X > DEFAULT_MAP_MAX ? FORM_DEFAULT_SIZE.Width + (X - DEFAULT_MAP_MAX) * MassBox.MASS_SIZE.Width : FORM_DEFAULT_SIZE.Width,
                                 Y > DEFAULT_MAP_MAX ? FORM_DEFAULT_SIZE.Height + (Y - DEFAULT_MAP_MAX) * MassBox.MASS_SIZE.Height : FORM_DEFAULT_SIZE.Height);

            MapBox.Size = new Size(X > DEFAULT_MAP_MAX ? MAPBOX_DEFAULT_SIZE.Width + (X - DEFAULT_MAP_MAX) * MassBox.MASS_SIZE.Width : MAPBOX_DEFAULT_SIZE.Width,
                                   Y > DEFAULT_MAP_MAX ? MAPBOX_DEFAULT_SIZE.Height + (Y - DEFAULT_MAP_MAX) * MassBox.MASS_SIZE.Height : MAPBOX_DEFAULT_SIZE.Height);

            PropertySelecter.Location = new Point(X > DEFAULT_MAP_MAX ? BASE_SPACE.X / 2 + PROPARTY_DEFAULT_POS.X + (X - DEFAULT_MAP_MAX) * MassBox.MASS_SIZE.Width : PROPARTY_DEFAULT_POS.X,
                                                  PROPARTY_DEFAULT_POS.Y);

            LoadMapWalls();
            MassBox.AllowDrag(true);
        }

        /// <summary>
        /// 壁データをロード
        /// </summary>
        private void LoadMapWalls()
        {
            if (massImage == null)
                massImage = Image.FromFile(CurrentPath + MASS_PATH);

            if (massBoxes == null || massBoxes.Length == 0)
                massBoxes = new MassBox[Y * X];
            for (int x = 0; x < X; x++)
            {
                for (int y = 0; y < Y; y++)
                {
                    if (massBoxes[x + y * X] == null)
                    {
                        massBoxes[x + y * X] = new MassBox(MapBox, x, y, massImage, () => SaveModeData());
                    }
                    var mass = massBoxes[x + y * X];
                    mass.Walls[UP].BackColor = mapDatas[XStartIndex + x + y * X] > 0 && mapDatas[XStartIndex + x + y * X] != 6 ? Color.Yellow : MassBox.DEFAULT_COLOR;
                    mass.Walls[RIGHT].BackColor = mapDatas[x + 1 + y * (X + 1)] > 0 && mapDatas[x + 1 + y * (X + 1)] != 7 ? Color.Yellow : MassBox.DEFAULT_COLOR;
                    mass.Walls[DOWN].BackColor = mapDatas[XStartIndex + x + (y + 1) * X] > 0 && mapDatas[XStartIndex + x + (y + 1) * X] != 7 ? Color.Yellow : MassBox.DEFAULT_COLOR;
                    mass.Walls[LEFT].BackColor = mapDatas[x + y * (X + 1)] > 0 && mapDatas[x + y * (X + 1)] != 6 ? Color.Yellow : MassBox.DEFAULT_COLOR;

                    mass.Doors[UP].BackColor = mapDatas[XStartIndex + x + y * X] == 8 || (mapDatas[XStartIndex + x + y * X] >= 2 && mapDatas[XStartIndex + x + y * X] <= 5) ? Color.Blue : MassBox.DEFAULT_COLOR;
                    mass.Doors[RIGHT].BackColor = mapDatas[x + 1 + y * (X + 1)] == 9 || (mapDatas[x + 1 + y * (X + 1)] >= 2 && mapDatas[x + 1 + y * (X + 1)] <= 5) ? Color.Blue : MassBox.DEFAULT_COLOR;
                    mass.Doors[DOWN].BackColor = mapDatas[XStartIndex + x + (y + 1) * X] == 9 || (mapDatas[XStartIndex + x + (y + 1) * X] >= 2 && mapDatas[XStartIndex + x + (y + 1) * X] <= 5) ? Color.Blue : MassBox.DEFAULT_COLOR;
                    mass.Doors[LEFT].BackColor = mapDatas[x + y * (X + 1)] == 8 || (mapDatas[x + y * (X + 1)] >= 2 && mapDatas[x + y * (X + 1)] <= 5) ? Color.Blue : MassBox.DEFAULT_COLOR;
                }
            }
        }

        /// <summary>
        /// ファイル出力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileExportButton_Click(object sender, EventArgs e)
        {
            if (massBoxes == null || massBoxes.Length == 0)
            {
                MessageBox.Show("マップがロードされていません。\nマップをロードしてください。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            saveFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            saveFileDialog1.Filter = "txt files (*.mdata)|*.mdata|All files (*.*)|*.*";
            DialogResult dr = saveFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                var filePath = saveFileDialog1.FileName;
                var text = GetExportText();
                if (File.Exists(filePath))
                    File.Delete(filePath);
                File.AppendAllText(filePath, text);
            }
        }

        /// <summary>
        /// 出力テキストを得る
        /// </summary>
        /// <returns></returns>
        private string GetExportText()
        {
            SaveModeData();

            string text = string.Empty;

            text += MAP_NUMFLOOR + NumFloor + '\n';
            text += MAPSIZE_X_LABEL + X.ToString() + '\n';
            text += MAPSIZE_Y_LABEL + Y.ToString() + '\n';
            for (int floor = 1; floor <= NumFloor; floor++)
            {
                text += '\n' + MAPFLOOR_LABEL + floor.ToString() + '\n';
                for (int i = 0; i < MODE_NUM; i++)
                {
                    text += TAGNAMES[i] + '\n';
                    for (int y = 0; y < Y; y++)
                    {
                        for (int x = 0; x < X; x++)
                        {
                            string str = string.Format("{0:X2}", modeData[floor - 1, i, x + y * X]);
                            text += str;
                        }
                        text += '\n';
                    }
                }
            }
            return text;
        }

        /// <summary>
        /// 現在のモードのテキストをキャッシュ
        /// </summary>
        private void SaveModeData()
        {
            for (int i = 0; i < massBoxes.Length; i++)
            {
                modeData[Floor - 1, mode, i] = Convert.ToByte(massBoxes[i].Text.Text, 16);
            }
        }

        /// <summary>
        /// モードごとのデータを反映
        /// </summary>
        private void LoadModeData()
        {
            for (int i = 0; i < massBoxes.Length; i++)
            {
                var num = modeData[Floor - 1, mode, i];
                massBoxes[i].Text.Text = string.Format("{0:X2}", num);
                if (num > 0)
                    massBoxes[i].Text.BackColor = MassBox.INPUTED_COLOR;
                else
                    massBoxes[i].Text.BackColor = MassBox.DEFAULT_COLOR;
            }
        }

        /// <summary>
        /// マップアイコンのラジオボタン変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mapIconIdRadio_AppearanceChanged(object sender, EventArgs e)
        {
            if (mapIconIdRadio.Checked)
            {
                SaveModeData();
                mode = 0;
                LoadModeData();
            }
        }

        /// <summary>
        /// 敵出現率のラジオボタン変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void enemyAppearRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (enemyAppearRadio.Checked)
            {
                SaveModeData();
                mode = 1;
                LoadModeData();
            }
        }

        /// <summary>
        /// どの敵グループを出すかモードラジオボタン変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void enemyLayerRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (enemyLayerRadio.Checked)
            {
                SaveModeData();
                mode = 2;
                LoadModeData();
            }
        }

        /// <summary>
        /// 会話更新ラジオボタン変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TalkRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (TalkRadio.Checked)
            {
                SaveModeData();
                mode = 3;
                LoadModeData();
            }
        }

        /// <summary>
        /// かんぬきマップラジオボタン変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void doorLockRadio_Click(object sender, EventArgs e)
        {
            if (doorLockRadio.Checked)
            {
                SaveModeData();
                mode = 4;
                LoadModeData();
            }
        }

        /// <summary>
        /// フロア変更時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void floorSelecter_ValueChanged(object sender, EventArgs e)
        {
            SaveModeData();
            Floor = (int)floorSelecter.Value;
            LoadMapData(Floor);
            LoadMapWalls();
            LoadModeData();
        }

        /// <summary>
        /// 出力ファイルからのロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fileLoadButton_Click_1(object sender, EventArgs e)
        {
            if (massBoxes == null || massBoxes.Length == 0)
            {
                MessageBox.Show("マップがロードされていません。\nマップをロードしてください。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            openFileDialog1.InitialDirectory = Directory.GetCurrentDirectory();
            openFileDialog1.Filter = "txt files (*.mdata)|*.mdata|All files (*.*)|*.*";
            DialogResult dr = openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                var datas = File.ReadAllLines(openFileDialog1.FileName);
                LoadVariableData(datas);
            }
        }

        /// <summary>
        /// 変数データをロード
        /// </summary>
        private void LoadVariableData(string[] datas)
        {
            if (X == -1 || Y == -1)
            {
                MessageBox.Show("マップがロードされていません。\nマップをロードしてください。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int index = 0;
            int floor = 0;
            while (index < datas.Length)
            {
                string line = datas[index];
                if (line.Contains(MAPSIZE_X_LABEL))
                {
                    int x = Convert.ToInt32(line.Replace(MAPSIZE_X_LABEL, string.Empty));
                    if (x != X)
                    {
                        MessageBox.Show("マップサイズが不正です。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (line.Contains(MAPSIZE_Y_LABEL))
                {
                    int y = Convert.ToInt32(line.Replace(MAPSIZE_Y_LABEL, string.Empty));
                    if (y != Y)
                    {
                        MessageBox.Show("マップサイズが不正です。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (line.Contains(MAPFLOOR_LABEL))
                {
                    floor = Convert.ToInt32(line.Replace(MAPFLOOR_LABEL, string.Empty));
                }
                for (int j = 0; j < MODE_NUM; j++)
                {
                    if (line.Contains(TAGNAMES[j]))
                    {
                        index = ReadLoadData(floor, j, index, datas);
                        if (index == -1)
                            return;
                        break;
                    }
                }
                index++;
            }
            Floor = (int)floorSelecter.Value;
            LoadMapData(Floor);
            LoadMapWalls();
            LoadModeData();
        }

        /// <summary>
        /// 変数データを読み込みセットする
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="mode"></param>
        /// <param name="index"></param>
        /// <param name="datas"></param>
        /// <returns></returns>
        private int ReadLoadData(int floor, int mode, int index, string[] datas) 
        {
            if (floor == 0)
            {
                MessageBox.Show("フロアが設定されていません。\n設定ファイルを確認してください。", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }

            floor--;
            index++;

            for (int y = 0; y < Y; y++)
            {
                string line = datas[index];
                for (int x = 0; x < X; x++)
                {
                    string str = line.Substring(x * 2, 2);
                    modeData[floor, mode, x + y * X] = Convert.ToByte(str, 16);
                }
                index++;
            }
            return --index;
        }

        /// <summary>
        /// アクティブ時ドラッグを有効化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Activated(object sender, EventArgs e) 
        {
            MassBox.AllowDrag(true);
        }

        /// <summary>
        /// コントロールを離れるときにドラッグを無効化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Leave(object sender, EventArgs e) 
        {
            MassBox.AllowDrag(false);
        }

        private VariableForm mapIdTableForm;
        private VariableForm enemyLayerForm;
        private VariableForm talkTableForm;

        /// <summary>
        /// マップIDとアイコンを結びつけるテーブルを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mapIdTableButton_Click(object sender, EventArgs e)
        {
            if (mapIdTableForm == null)
            {
                mapIdTableForm = new VariableForm(VariableForm.Mode.MapIcon, null, (int a, string b) => mapIdTableForm = null);
                CloseAct.FormOpenCommon(this, mapIdTableForm);
            }
        }

        /// <summary>
        /// 敵レイヤーテーブルを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void enemyLayerButton_Click(object sender, EventArgs e)
        {
            if (enemyLayerForm == null)
            {
                enemyLayerForm = new VariableForm(VariableForm.Mode.Layer, null, (int a, string b) => enemyLayerForm = null);
                CloseAct.FormOpenCommon(this, enemyLayerForm);
            }
        }

        /// <summary>
        /// 会話発生でどのイベントを発生させるか定義するテーブルを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void talkTableButton_Click(object sender, EventArgs e)
        {
            if (talkTableForm == null)
            {
                talkTableForm = new VariableForm(VariableForm.Mode.Talk, null, (int a, string b) => talkTableForm = null);
                CloseAct.FormOpenCommon(this, talkTableForm);
            }
        }
    }
}
