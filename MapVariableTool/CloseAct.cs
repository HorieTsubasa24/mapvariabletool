﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapVariableTool
{
    public static class CloseAct
    {
        public static List<Form> currentForm = new List<Form>();

        /// <summary>
        /// 子フォームを開くときの共通処理。
        /// </summary>
        /// <param name="me_fm"></param>
        /// <param name="fm"></param>
        static public void FormOpenCommon(Form me_fm, Form fm)
        {
            currentForm.Add(me_fm);
            fm.FormClosed += new FormClosedEventHandler(FormCloseCommon);
            fm.Show(); // 子ﾌｫｰﾑを表示
            //currentForm[currentForm.Count - 1].Enabled = false;
        }

        /// <summary>
        /// スタックに保存した親フォームを、子フォームClose時にEnableさせる。 -> なにもしない
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static private void FormCloseCommon(object sender, FormClosedEventArgs e)
        {
            //currentForm[currentForm.Count - 1].Enabled = true;
            currentForm.RemoveAt(currentForm.Count - 1);
        }
    }
}
