﻿namespace MapVariableTool
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.mapLoadButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.PropertySelecter = new System.Windows.Forms.GroupBox();
            this.talkTableButton = new System.Windows.Forms.Button();
            this.enemyLayerButton = new System.Windows.Forms.Button();
            this.mapIdTableButton = new System.Windows.Forms.Button();
            this.doorLockRadio = new System.Windows.Forms.RadioButton();
            this.fileExportButton = new System.Windows.Forms.Button();
            this.fileLoadButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.selectFloorLabel = new System.Windows.Forms.Label();
            this.floorSelecter = new System.Windows.Forms.NumericUpDown();
            this.TalkRadio = new System.Windows.Forms.RadioButton();
            this.enemyAppearRadio = new System.Windows.Forms.RadioButton();
            this.mapIconIdRadio = new System.Windows.Forms.RadioButton();
            this.enemyLayerRadio = new System.Windows.Forms.RadioButton();
            this.MapBox = new System.Windows.Forms.GroupBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.PropertySelecter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floorSelecter)).BeginInit();
            this.SuspendLayout();
            // 
            // mapLoadButton
            // 
            this.mapLoadButton.Location = new System.Drawing.Point(20, 18);
            this.mapLoadButton.Name = "mapLoadButton";
            this.mapLoadButton.Size = new System.Drawing.Size(75, 23);
            this.mapLoadButton.TabIndex = 5;
            this.mapLoadButton.Text = "mapLoad";
            this.mapLoadButton.UseVisualStyleBackColor = true;
            this.mapLoadButton.Click += new System.EventHandler(this.fileLoadButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // PropertySelecter
            // 
            this.PropertySelecter.Controls.Add(this.talkTableButton);
            this.PropertySelecter.Controls.Add(this.enemyLayerButton);
            this.PropertySelecter.Controls.Add(this.mapIdTableButton);
            this.PropertySelecter.Controls.Add(this.doorLockRadio);
            this.PropertySelecter.Controls.Add(this.fileExportButton);
            this.PropertySelecter.Controls.Add(this.fileLoadButton);
            this.PropertySelecter.Controls.Add(this.mapLoadButton);
            this.PropertySelecter.Controls.Add(this.label1);
            this.PropertySelecter.Controls.Add(this.selectFloorLabel);
            this.PropertySelecter.Controls.Add(this.floorSelecter);
            this.PropertySelecter.Controls.Add(this.TalkRadio);
            this.PropertySelecter.Controls.Add(this.enemyAppearRadio);
            this.PropertySelecter.Controls.Add(this.mapIconIdRadio);
            this.PropertySelecter.Controls.Add(this.enemyLayerRadio);
            this.PropertySelecter.Location = new System.Drawing.Point(518, 12);
            this.PropertySelecter.Name = "PropertySelecter";
            this.PropertySelecter.Size = new System.Drawing.Size(132, 500);
            this.PropertySelecter.TabIndex = 1;
            this.PropertySelecter.TabStop = false;
            this.PropertySelecter.Text = "PropertySelecter";
            // 
            // talkTableButton
            // 
            this.talkTableButton.Location = new System.Drawing.Point(6, 397);
            this.talkTableButton.Name = "talkTableButton";
            this.talkTableButton.Size = new System.Drawing.Size(120, 36);
            this.talkTableButton.TabIndex = 13;
            this.talkTableButton.Text = "会話発生テーブルを\r\n開く";
            this.talkTableButton.UseVisualStyleBackColor = true;
            this.talkTableButton.Click += new System.EventHandler(this.talkTableButton_Click);
            // 
            // enemyLayerButton
            // 
            this.enemyLayerButton.Location = new System.Drawing.Point(6, 351);
            this.enemyLayerButton.Name = "enemyLayerButton";
            this.enemyLayerButton.Size = new System.Drawing.Size(120, 40);
            this.enemyLayerButton.TabIndex = 12;
            this.enemyLayerButton.Text = "敵レイヤーテーブルを\r\n開く";
            this.enemyLayerButton.UseVisualStyleBackColor = true;
            this.enemyLayerButton.Click += new System.EventHandler(this.enemyLayerButton_Click);
            // 
            // mapIdTableButton
            // 
            this.mapIdTableButton.Location = new System.Drawing.Point(6, 322);
            this.mapIdTableButton.Name = "mapIdTableButton";
            this.mapIdTableButton.Size = new System.Drawing.Size(120, 23);
            this.mapIdTableButton.TabIndex = 11;
            this.mapIdTableButton.Text = "マップIDテーブルを開く";
            this.mapIdTableButton.UseVisualStyleBackColor = true;
            this.mapIdTableButton.Click += new System.EventHandler(this.mapIdTableButton_Click);
            // 
            // doorLockRadio
            // 
            this.doorLockRadio.AutoSize = true;
            this.doorLockRadio.Location = new System.Drawing.Point(20, 216);
            this.doorLockRadio.Name = "doorLockRadio";
            this.doorLockRadio.Size = new System.Drawing.Size(87, 16);
            this.doorLockRadio.TabIndex = 6;
            this.doorLockRadio.Text = "かんぬきマップ";
            this.doorLockRadio.UseVisualStyleBackColor = true;
            this.doorLockRadio.Click += new System.EventHandler(this.doorLockRadio_Click);
            // 
            // fileExportButton
            // 
            this.fileExportButton.Location = new System.Drawing.Point(20, 76);
            this.fileExportButton.Name = "fileExportButton";
            this.fileExportButton.Size = new System.Drawing.Size(75, 23);
            this.fileExportButton.TabIndex = 10;
            this.fileExportButton.Text = "fileExport";
            this.fileExportButton.UseVisualStyleBackColor = true;
            this.fileExportButton.Click += new System.EventHandler(this.fileExportButton_Click);
            // 
            // fileLoadButton
            // 
            this.fileLoadButton.Location = new System.Drawing.Point(20, 47);
            this.fileLoadButton.Name = "fileLoadButton";
            this.fileLoadButton.Size = new System.Drawing.Size(75, 23);
            this.fileLoadButton.TabIndex = 9;
            this.fileLoadButton.Text = "fileLoad";
            this.fileLoadButton.UseVisualStyleBackColor = true;
            this.fileLoadButton.Click += new System.EventHandler(this.fileLoadButton_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 282);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "1~";
            // 
            // selectFloorLabel
            // 
            this.selectFloorLabel.AutoSize = true;
            this.selectFloorLabel.Location = new System.Drawing.Point(18, 259);
            this.selectFloorLabel.Name = "selectFloorLabel";
            this.selectFloorLabel.Size = new System.Drawing.Size(55, 12);
            this.selectFloorLabel.TabIndex = 7;
            this.selectFloorLabel.Text = "選択フロア";
            // 
            // floorSelecter
            // 
            this.floorSelecter.Location = new System.Drawing.Point(20, 280);
            this.floorSelecter.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.floorSelecter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.floorSelecter.Name = "floorSelecter";
            this.floorSelecter.Size = new System.Drawing.Size(75, 19);
            this.floorSelecter.TabIndex = 8;
            this.floorSelecter.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.floorSelecter.ValueChanged += new System.EventHandler(this.floorSelecter_ValueChanged);
            // 
            // TalkRadio
            // 
            this.TalkRadio.AutoSize = true;
            this.TalkRadio.Location = new System.Drawing.Point(20, 194);
            this.TalkRadio.Name = "TalkRadio";
            this.TalkRadio.Size = new System.Drawing.Size(71, 16);
            this.TalkRadio.TabIndex = 5;
            this.TalkRadio.Text = "会話発生";
            this.TalkRadio.UseVisualStyleBackColor = true;
            this.TalkRadio.CheckedChanged += new System.EventHandler(this.TalkRadio_CheckedChanged);
            // 
            // enemyAppearRadio
            // 
            this.enemyAppearRadio.AutoSize = true;
            this.enemyAppearRadio.Location = new System.Drawing.Point(20, 150);
            this.enemyAppearRadio.Name = "enemyAppearRadio";
            this.enemyAppearRadio.Size = new System.Drawing.Size(71, 16);
            this.enemyAppearRadio.TabIndex = 3;
            this.enemyAppearRadio.Text = "敵出現率";
            this.enemyAppearRadio.UseVisualStyleBackColor = true;
            this.enemyAppearRadio.CheckedChanged += new System.EventHandler(this.enemyAppearRadio_CheckedChanged);
            // 
            // mapIconIdRadio
            // 
            this.mapIconIdRadio.AutoSize = true;
            this.mapIconIdRadio.Checked = true;
            this.mapIconIdRadio.Location = new System.Drawing.Point(20, 128);
            this.mapIconIdRadio.Name = "mapIconIdRadio";
            this.mapIconIdRadio.Size = new System.Drawing.Size(94, 16);
            this.mapIconIdRadio.TabIndex = 2;
            this.mapIconIdRadio.TabStop = true;
            this.mapIconIdRadio.Text = "マップアイコンID";
            this.mapIconIdRadio.UseVisualStyleBackColor = true;
            this.mapIconIdRadio.AppearanceChanged += new System.EventHandler(this.mapIconIdRadio_AppearanceChanged);
            this.mapIconIdRadio.CheckedChanged += new System.EventHandler(this.mapIconIdRadio_AppearanceChanged);
            // 
            // enemyLayerRadio
            // 
            this.enemyLayerRadio.AutoSize = true;
            this.enemyLayerRadio.Location = new System.Drawing.Point(20, 172);
            this.enemyLayerRadio.Name = "enemyLayerRadio";
            this.enemyLayerRadio.Size = new System.Drawing.Size(97, 16);
            this.enemyLayerRadio.TabIndex = 4;
            this.enemyLayerRadio.Text = "敵出現レイヤー";
            this.enemyLayerRadio.UseVisualStyleBackColor = true;
            this.enemyLayerRadio.CheckedChanged += new System.EventHandler(this.enemyLayerRadio_CheckedChanged);
            // 
            // MapBox
            // 
            this.MapBox.BackColor = System.Drawing.Color.MediumPurple;
            this.MapBox.Location = new System.Drawing.Point(12, 12);
            this.MapBox.Name = "MapBox";
            this.MapBox.Size = new System.Drawing.Size(500, 500);
            this.MapBox.TabIndex = 0;
            this.MapBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(674, 524);
            this.Controls.Add(this.PropertySelecter);
            this.Controls.Add(this.MapBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "マップ値編集";
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Deactivate += new System.EventHandler(this.MainForm_Leave);
            this.Enter += new System.EventHandler(this.MainForm_Activated);
            this.Leave += new System.EventHandler(this.MainForm_Leave);
            this.PropertySelecter.ResumeLayout(false);
            this.PropertySelecter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.floorSelecter)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button mapLoadButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox PropertySelecter;
        private System.Windows.Forms.RadioButton enemyAppearRadio;
        private System.Windows.Forms.RadioButton mapIconIdRadio;
        private System.Windows.Forms.RadioButton TalkRadio;
        private System.Windows.Forms.GroupBox MapBox;
        private System.Windows.Forms.Label selectFloorLabel;
        private System.Windows.Forms.NumericUpDown floorSelecter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button fileLoadButton;
        private System.Windows.Forms.Button fileExportButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.RadioButton enemyLayerRadio;
        private System.Windows.Forms.RadioButton doorLockRadio;
        private System.Windows.Forms.Button mapIdTableButton;
        private System.Windows.Forms.Button talkTableButton;
        private System.Windows.Forms.Button enemyLayerButton;
    }
}

